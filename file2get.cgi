#!/bin/bash

# file2get.cgi
#
# Recives tumblr post URL and return its photo
#
# @author     pilasguru 
# @copyright  2017/2014 Rodolfo Pilas
# @license    http://opensource.org/licenses/MIT  The MIT License (MIT)
# @link       https://gitlab.com/pilasguru/tumblr-photo-url 


URL=$(echo $QUERY_STRING | cut -d'=' -f2)
URLGET=$(echo $URL | sed "s/post/mobile\/post/")
URLFILE=$(/usr/bin/curl -s $URLGET | \
		/bin/grep "<a href=\".*media" | \
		/usr/bin/cut -d \" -f 2)

case "$URLFILE" in
    *.png)	echo "Content-Type: image/png";;
    *.jpe)	echo "Content-Type: image/jpeg";;
    *.jpeg)	echo "Content-Type: image/jpeg";;
    *.jpg)	echo "Content-Type: image/jpeg";;
    *.gif)	echo "Content-Type: image/gif";;
esac
echo 'Content-Disposition: attachment; filename="'`echo ${URLFILE##h*/}`'"'
echo ""
/usr/bin/curl -s $URLFILE
