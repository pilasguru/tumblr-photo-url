tumblr-photo-url
================

Simple CGI that receives a tumblr post URL and return its **raw photo**

```
This script do not works with video content
```

Usage
-----

1. Put `file2get.cgi` at any webserver **cgi folder** folder
2. http://your.server/cgi-bin/file2get.cgi?u=http://evanpalmercomics.tumblr.com/post/69670312160

## History

* 2017 use `curl` command to get data and moved to bash script as CGI (`file2get.cgi`)
* 2014 to 2016 PHP script (`tumblr2.php`) but tumblr avoid send data to simple GET request.

Credits
-------

Based on Motyar code http://motyar.blogspot.com/2013/02/hacking-ifttt.html 
